[![GitHub license](https://img.shields.io/badge/license-apache-bluc.svg)](https://bitbucket.org/nowhh01/dynamicstylecomponentexample/src/main/LICENSE)

# License

## Permissions
![license commecial](https://img.shields.io/badge/-Commecial%20use-green)
![license modification](https://img.shields.io/badge/-Modification-green)
![license distribution](https://img.shields.io/badge/-Distribution-green)
![license patent](https://img.shields.io/badge/-Patent%20use-green)
![license private](https://img.shields.io/badge/-Private%20use-green)

## Limitations
![license trademark](https://img.shields.io/badge/-Trademark%20use-red)
![license liability](https://img.shields.io/badge/-Liability-red)
![license warranty](https://img.shields.io/badge/-Warranty-red)


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm -v` or `yarn -v` in command prompt or terminal

If you don't have any of them, install one

### `npm install` or `yarn install`

To install dependencies

### `npm start` or `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


