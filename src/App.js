import React, { cloneElement } from 'react';
import {
  Box,
  Card,
  CardContent,
  Container,
  Grid,
  Typography,
  Paper,
  List,
} from '@material-ui/core';

import {
  AcUnit,
  Assignment,
  Cloud,
  DirectionsBus,
  DirectionsTransit,
  Facebook,
  MailOutline,
  MeetingRoom,
  Voicemail,
  WhatsApp,
} from '@material-ui/icons';

const DynamicStyleComponent = ({ component, style }) => {
  return cloneElement(component, { style: { ...style } });
};

const IconBox = ({ children }) => {
  return (
    <Box
      style={{
        border: '2px solid lightgray',
        borderRadius: 25,
        marginTop: 10,
        width: '20vw',
        height: '10vh',
        minWidth: 160,
        minHeight: 60,
        maxWidth: 200,
        maxHeight: 160,
      }}
    >
      <Grid container direction="column" style={{ paddingTop: 'max(3px, 2%)' }}>
        {children}
      </Grid>
    </Box>
  );
};

const Icon = ({ icon, text, color }) => {
  return (
    <Container
      style={{
        display: 'flex',
        paddingLeft: '15px',
      }}
    >
      <DynamicStyleComponent
        component={icon}
        style={{ fontSize: 'max(min(4.6vh, 46px), 26px)', color: color }}
      />
      <Box
        component="span"
        style={{
          alignSelf: 'center',
          fontSize: 'max(min(1.8vw, 20px), 14px)',
          color: 'gray',
        }}
      >
        {text}
      </Box>
    </Container>
  );
};

function App() {
  return (
    <Grid
      container
      spacing={0}
      alignItems="center"
      justify="center"
      style={{
        minHeight: '100vh',
        minWidth: '100vw',
      }}
    >
      <Container maxWidth="lg" style={{ display: 'flex' }}>
        <Container
          style={{
            alignSelf: 'center',
            textAlign: '-webkit-right',
          }}
        >
          <IconBox>
            <Icon icon={<Cloud />} text="-10°C" color="lightblue"></Icon>
            <Icon icon={<AcUnit />} text="From 2pm" color="lightblue"></Icon>
          </IconBox>
          <IconBox>
            <Icon icon={<DirectionsTransit />} text="8:10 am"></Icon>
            <Icon icon={<DirectionsBus />} text="8:15 am"></Icon>
          </IconBox>
          <IconBox>
            <Icon icon={<Facebook />} text="2 Messages" color="blue"></Icon>
            <Icon icon={<WhatsApp />} text="6 Messages" color="green"></Icon>
          </IconBox>
        </Container>

        {/* center box */}
        <Card
          style={{
            borderRadius: 25,
            width: '250vw',
            height: '50vh',
            minWidth: 320,
            minHeight: 240,
          }}
          elevation={15}
        >
          <CardContent></CardContent>
        </Card>

        <Container
          style={{
            alignSelf: 'center',
            textAlign: '-webkit-left',
          }}
        >
          <IconBox>
            <Icon icon={<MailOutline />} text="5 Emails" color="red"></Icon>
            <Icon icon={<Voicemail />} text="2 Voicemails"></Icon>
          </IconBox>
          <IconBox>
            <Icon icon={<Assignment />} text="Send Email" color="brown"></Icon>
            <Icon
              icon={<Assignment />}
              text="Documentation"
              color="brown"
            ></Icon>
          </IconBox>{' '}
          <IconBox>
            <Icon icon={<MeetingRoom />} text="11:00 am" color="coral"></Icon>
            <Icon icon={<MeetingRoom />} text="02:00 pm" color="coral"></Icon>
          </IconBox>
        </Container>
      </Container>
    </Grid>
  );
}

export default App;
